package com.besprime.web.resource;

import com.besprime.root.entity.Customer;
import com.besprime.root.entity.CustomerOrder;
import com.besprime.root.entity.ShippingAddress;
import com.besprime.root.response.Response;
import com.besprime.root.service.interfaces.CustomerService;
import com.besprime.root.util.Constants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

@RestController
public class CustomerResource extends AbstractResource {
    public static final Logger LOGGER = Logger.getLogger(CustomerResource.class);

    @Autowired
    private CustomerService customerService;

    public CustomerService getCustomerService() {
        return customerService;
    }

    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @RequestMapping(value = "/customer",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> fetchAllCustomers(final @RequestHeader(name = "Authorization", required = false)
                                                       String authorization,
                                               final @RequestHeader(name = "Content-Type", required = false)
                                                       String contentType) {
        if (getValidator().validate(
                new String[]{
                        authorization,
                        contentType}
        ) && contentType.equals(Constants.CONTENT_TYPE_APPLICATION_JSON)) {
            LOGGER.debug(Constants.STATUS_REQ_ENTRY);

            String credentials = getCredentialManager()
                    .decodeBase64(authorization);

            List<String> nameAndEmail = Arrays
                    .asList(credentials.split(":"));

            Customer getter = getCustomerService().
                    findCustomerByNameAndEmail(nameAndEmail.get(0), nameAndEmail.get(1));

            if (getter != null) {
                LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.GETTER_FOUND));

                List<Customer> list = getCustomerService().findAll();

                if (list == null || list.isEmpty()) {
                    LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.NO_CUSTOMER_FOUND)
                            .concat(" http status = ").concat(HttpStatus.NOT_FOUND.name()));

                    Response response = generateResponse(Constants.NO_CUSTOMER_FOUND);

                    return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
                } else {
                    LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.SUCCESS)
                            .concat(" http status = ").concat(HttpStatus.OK.name()));

                    return new ResponseEntity<>(list, HttpStatus.OK);
                }
            } else {
                LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.NO_GETTER_FOUND)
                        .concat(" http status = ").concat(HttpStatus.CONFLICT.name()));

                Response response = generateResponse(Constants.NO_GETTER_FOUND);

                return new ResponseEntity<>(response, HttpStatus.CONFLICT);
            }
        }
        LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.ERROR)
                .concat(" http status = ").concat(HttpStatus.BAD_REQUEST.name()));

        Response response = generateResponse(Constants.ERROR);

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/customer/{customerName}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> fetchCustomersByName(final @RequestHeader(name = "Authorization", required = false)
                                                          String authorization,
                                                  final @RequestHeader(name = "Content-Type", required = false)
                                                          String contentType,
                                                  final @PathVariable String customerName) {
        if (getValidator().validate(
                new String[]{
                        authorization,
                        contentType,
                        customerName}
        ) && contentType.equals(Constants.CONTENT_TYPE_APPLICATION_JSON)) {
            LOGGER.debug(Constants.STATUS_REQ_ENTRY);

            String credentials = getCredentialManager()
                    .decodeBase64(authorization);

            List<String> nameAndEmail = Arrays
                    .asList(credentials.split(":"));

            Customer getter = getCustomerService().
                    findCustomerByNameAndEmail(nameAndEmail.get(0), nameAndEmail.get(1));

            if (getter != null) {
                LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.GETTER_FOUND));

                Customer resultCustomer = getCustomerService().findCustomerByName(customerName);

                if (resultCustomer == null) {
                    LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.NO_CUSTOMER_FOUND)
                            .concat(" http status = ").concat(HttpStatus.NOT_FOUND.name()));

                    Response response = generateResponse(Constants.NO_CUSTOMER_FOUND);

                    return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
                } else {
                    LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.SUCCESS)
                            .concat(" http status = ").concat(HttpStatus.OK.name()));

                    return new ResponseEntity<>(resultCustomer, HttpStatus.OK);
                }
            } else {
                LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.NO_GETTER_FOUND)
                        .concat(" http status = ").concat(HttpStatus.CONFLICT.name()));

                Response response = generateResponse(Constants.NO_GETTER_FOUND);

                return new ResponseEntity<>(response, HttpStatus.CONFLICT);
            }
        }
        LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.ERROR)
                .concat(" http status = ").concat(HttpStatus.BAD_REQUEST.name()));

        Response response = generateResponse(Constants.ERROR);

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/customer/{customerName}/shippingAddress",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> fetchCustomersShippingAddresses(final @RequestHeader(name = "Authorization", required = false)
                                                                     String authorization,
                                                             final @RequestHeader(name = "Content-Type", required = false)
                                                                     String contentType,
                                                             final @PathVariable String customerName) {
        if (getValidator().validate(
                new String[]{
                        authorization,
                        contentType,
                        customerName}
        ) && contentType.equals(Constants.CONTENT_TYPE_APPLICATION_JSON)) {
            LOGGER.debug(Constants.STATUS_REQ_ENTRY);

            String credentials = getCredentialManager()
                    .decodeBase64(authorization);

            List<String> nameAndEmail = Arrays
                    .asList(credentials.split(":"));

            Customer getter = getCustomerService().
                    findCustomerByNameAndEmail(nameAndEmail.get(0), nameAndEmail.get(1));

            if (getter != null) {
                LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.GETTER_FOUND));

                Customer resultCustomer = getCustomerService().findCustomerByName(customerName);

                if (resultCustomer == null) {
                    LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.NO_CUSTOMER_FOUND)
                            .concat(" http status = ").concat(HttpStatus.NOT_FOUND.name()));

                    Response response = generateResponse(Constants.NO_CUSTOMER_FOUND);

                    return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
                } else {
                    LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.SUCCESS)
                            .concat(" http status = ").concat(HttpStatus.OK.name()));

                    Set<ShippingAddress> shippingAddresses = resultCustomer.getShippingAddresses();

                    if (shippingAddresses != null && !shippingAddresses.isEmpty()) {
                        LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.SUCCESS)
                                .concat(" http status = ").concat(HttpStatus.OK.name()));

                        return new ResponseEntity<>(shippingAddresses, HttpStatus.OK);
                    } else {
                        LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.NO_SHIPPING_ADDRESS_FOUND)
                                .concat(" http status = ").concat(HttpStatus.CONFLICT.name()));

                        Response response = generateResponse(Constants.NO_SHIPPING_ADDRESS_FOUND);

                        return new ResponseEntity<>(response, HttpStatus.CONFLICT);
                    }
                }
            } else {
                LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.NO_GETTER_FOUND)
                        .concat(" http status = ").concat(HttpStatus.CONFLICT.name()));

                Response response = generateResponse(Constants.NO_GETTER_FOUND);

                return new ResponseEntity<>(response, HttpStatus.CONFLICT);
            }
        }
        LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.ERROR)
                .concat(" http status = ").concat(HttpStatus.BAD_REQUEST.name()));

        Response response = generateResponse(Constants.ERROR);

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/customer/{customerName}/customerOrder",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> fetchCustomersOrders(final @RequestHeader(name = "Authorization", required = false)
                                                          String authorization,
                                                  final @RequestHeader(name = "Content-Type", required = false)
                                                          String contentType,
                                                  final @PathVariable String customerName) {
        if (getValidator().validate(
                new String[]{
                        authorization,
                        contentType,
                        customerName}
        ) && contentType.equals(Constants.CONTENT_TYPE_APPLICATION_JSON)) {
            LOGGER.debug(Constants.STATUS_REQ_ENTRY);

            String credentials = getCredentialManager()
                    .decodeBase64(authorization);

            List<String> nameAndEmail = Arrays
                    .asList(credentials.split(":"));

            Customer getter = getCustomerService().
                    findCustomerByNameAndEmail(nameAndEmail.get(0), nameAndEmail.get(1));

            if (getter != null) {
                LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.GETTER_FOUND));

                Customer resultCustomer = getCustomerService().findCustomerByName(customerName);

                if (resultCustomer == null) {
                    LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.NO_CUSTOMER_FOUND)
                            .concat(" http status = ").concat(HttpStatus.NOT_FOUND.name()));

                    Response response = generateResponse(Constants.NO_CUSTOMER_FOUND);

                    return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
                } else {
                    LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.SUCCESS)
                            .concat(" http status = ").concat(HttpStatus.OK.name()));

                    Set<CustomerOrder> customerOrders = resultCustomer.getCustomerOrders();

                    if (customerOrders != null && !customerOrders.isEmpty()) {
                        LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.SUCCESS)
                                .concat(" http status = ").concat(HttpStatus.OK.name()));

                        return new ResponseEntity<>(customerOrders, HttpStatus.OK);
                    } else {
                        LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.NO_CUSTOMER_ORDERS_FOUND)
                                .concat(" http status = ").concat(HttpStatus.CONFLICT.name()));

                        Response response = generateResponse(Constants.NO_CUSTOMER_ORDERS_FOUND);

                        return new ResponseEntity<>(response, HttpStatus.CONFLICT);
                    }
                }
            } else {
                LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.NO_GETTER_FOUND)
                        .concat(" http status = ").concat(HttpStatus.CONFLICT.name()));

                Response response = generateResponse(Constants.NO_GETTER_FOUND);

                return new ResponseEntity<>(response, HttpStatus.CONFLICT);
            }
        }
        LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.ERROR)
                .concat(" http status = ").concat(HttpStatus.BAD_REQUEST.name()));

        Response response = generateResponse(Constants.ERROR);

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
