package com.besprime.web.resource;

import com.besprime.root.entity.Customer;
import com.besprime.root.entity.CustomerOrder;
import com.besprime.root.entity.StockKeepingUnit;
import com.besprime.root.entity.StockKeepingUnitStock;
import com.besprime.root.response.CheckoutRequest;
import com.besprime.root.response.CheckoutResponse;
import com.besprime.root.response.Response;
import com.besprime.root.service.interfaces.CustomerOrderService;
import com.besprime.root.service.interfaces.CustomerService;
import com.besprime.root.service.interfaces.StockKeepingUnitService;
import com.besprime.root.util.Constants;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class CustomerOrderResource extends AbstractResource {
    public static final Logger LOGGER = Logger.getLogger(CustomerOrderResource.class);

    @Autowired
    private CustomerService customerService;

    @Autowired
    private StockKeepingUnitService stockKeepingUnitService;

    @Autowired
    private CustomerOrderService customerOrderService;

    public CustomerService getCustomerService() {
        return customerService;
    }

    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    public StockKeepingUnitService getStockKeepingUnitService() {
        return stockKeepingUnitService;
    }

    public void setStockKeepingUnitService(StockKeepingUnitService stockKeepingUnitService) {
        this.stockKeepingUnitService = stockKeepingUnitService;
    }

    public CustomerOrderService getCustomerOrderService() {
        return customerOrderService;
    }

    public void setCustomerOrderService(CustomerOrderService customerOrderService) {
        this.customerOrderService = customerOrderService;
    }

    @RequestMapping(value = "/customerOrder",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> createOrder(final @RequestHeader(name = "Authorization", required = false)
                                                 String authorization,
                                         final @RequestHeader(name = "Content-Type", required = false)
                                                 String contentType,
                                         final @RequestBody String body) {
        if (getValidator().validate(
                new String[]{
                        authorization,
                        contentType}
        ) && contentType.equals(Constants.CONTENT_TYPE_APPLICATION_JSON)) {
            LOGGER.debug(Constants.STATUS_REQ_ENTRY);

            String credentials = getCredentialManager()
                    .decodeBase64(authorization);

            List<String> nameAndEmail = Arrays
                    .asList(credentials.split(":"));

            Customer getter = getCustomerService().
                    findCustomerByNameAndEmail(nameAndEmail.get(0), nameAndEmail.get(1));

            if (getter != null) {
                LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.GETTER_FOUND));

                ObjectMapper mapper = new ObjectMapper();
                try {
                    CheckoutRequest requestJson = mapper.readValue(body, new TypeReference<CheckoutRequest>() {
                    });

                    List<StockKeepingUnit> realSKU = new ArrayList<>();
                    List<Long> quantityRealSKU = new ArrayList<>();
                    List<String> invalidSKU = new ArrayList<>();
                    List<String> lessSKU = new ArrayList<>();

                    //existing and non-existing SKUs
                    for (Map.Entry<String, Long> entry : requestJson.getStockKeepingUnits().entrySet()) {
                        StockKeepingUnit stockKeepingUnit = getStockKeepingUnitService().findByName(entry.getKey());
                        if (stockKeepingUnit != null) {
                            realSKU.add(stockKeepingUnit);
                            quantityRealSKU.add(entry.getValue());
                        } else {
                            invalidSKU.add(entry.getKey());
                        }
                    }

                    //total count of existing SKUs in all stocks
                    List<Long> totalSKUInStocks = new ArrayList<>();
                    for (StockKeepingUnit item : realSKU) {
                        Long sum = 0L;
                        for (StockKeepingUnitStock stockKeepingUnitStock : item.getStockKeepingUnitStocks()) {
                            sum = sum + stockKeepingUnitStock.getAmount();
                        }
                        totalSKUInStocks.add(sum);
                    }

                    //total price of real SKUs
                    Long totalPrice = 0L;
                    for (int i = 0; i < realSKU.size(); i++) {
                        Long amount = requestJson.getStockKeepingUnits().get(realSKU.get(i).getName());
                        if (amount <= totalSKUInStocks.get(i)) {
                            totalPrice = totalPrice + realSKU.get(i).getPrice() * amount;
                        } else {
                            lessSKU.add(realSKU.get(i).getName());
                        }
                    }

                    if (getter.getCredit() >= totalPrice) {
                        CheckoutResponse checkoutResponse = process(realSKU,
                                quantityRealSKU,
                                invalidSKU,
                                lessSKU,
                                totalPrice,
                                getter);

                        LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.SUCCESS)
                                .concat(" http status = ").concat(HttpStatus.OK.name()));

                        return new ResponseEntity<>(checkoutResponse, HttpStatus.OK);
                    } else {
                        LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat("No enough credit")
                                .concat(" http status = ").concat(HttpStatus.CONFLICT.name()));

                        Response response = generateResponse("No enough credit");

                        return new ResponseEntity<>(response, HttpStatus.CONFLICT);
                    }
                } catch (Exception ex) {
                    LOGGER.error(ex, ex);

                    Response response = generateResponse(ex.getMessage());

                    return new ResponseEntity<>(response, HttpStatus.EXPECTATION_FAILED);
                }
            } else {
                LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.NO_GETTER_FOUND)
                        .concat(" http status = ").concat(HttpStatus.CONFLICT.name()));

                Response response = generateResponse(Constants.NO_GETTER_FOUND);

                return new ResponseEntity<>(response, HttpStatus.CONFLICT);
            }
        }
        LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.ERROR)
                .concat(" http status = ").concat(HttpStatus.BAD_REQUEST.name()));

        Response response = generateResponse(Constants.ERROR);

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    private CheckoutResponse process(final List<StockKeepingUnit> realSKU,
                                     final List<Long> quantitySKU,
                                     final List<String> invalidSKU,
                                     final List<String> lessSKU,
                                     final Long totalPrice,
                                     final Customer customer) {
        CheckoutResponse checkoutResponse = new CheckoutResponse();
        checkoutResponse.setValidSKU(realSKU);
        checkoutResponse.setInvalidSKU(invalidSKU);
        checkoutResponse.setLessSKU(lessSKU);
        checkoutResponse.setTotalAmount(totalPrice);

        CustomerOrder customerOrder = new CustomerOrder();
        customerOrder.setStockKeepingUnits(realSKU);
        customerOrder.setCustomer(customer);

        getCustomerOrderService().save(customerOrder);

        customer.setCredit(customer.getCredit() - totalPrice);
        getCustomerService().save(customer);

        for (int i = 0; i < realSKU.size(); i++) {
            if (!lessSKU.contains(realSKU.get(i).getName())) {
                Set<StockKeepingUnitStock> stockKeepingUnitStocks = realSKU.get(i).getStockKeepingUnitStocks();
                Long restOfSize = quantitySKU.get(i);

                for (StockKeepingUnitStock skus : stockKeepingUnitStocks) {
                    if (skus.getAmount() >= restOfSize) {
                        skus.setAmount(skus.getAmount() - restOfSize);
                        break;
                    } else {
                        restOfSize = restOfSize - skus.getAmount();
                        skus.setAmount(0L);
                    }
                }

                getStockKeepingUnitService().save(realSKU.get(i));
            }
        }

        return checkoutResponse;
    }

    @RequestMapping(value = "/customerOrder",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> fetchAllCustomerOrders(final @RequestHeader(name = "Authorization", required = false)
                                                            String authorization,
                                                    final @RequestHeader(name = "Content-Type", required = false)
                                                            String contentType) {
        if (getValidator().validate(
                new String[]{
                        authorization,
                        contentType}
        ) && contentType.equals(Constants.CONTENT_TYPE_APPLICATION_JSON)) {
            LOGGER.debug(Constants.STATUS_REQ_ENTRY);

            String credentials = getCredentialManager()
                    .decodeBase64(authorization);

            List<String> nameAndEmail = Arrays
                    .asList(credentials.split(":"));

            Customer getter = getCustomerService().
                    findCustomerByNameAndEmail(nameAndEmail.get(0), nameAndEmail.get(1));

            if (getter != null) {
                LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.GETTER_FOUND));

                List<CustomerOrder> list = getCustomerOrderService().findAll();

                if (list == null || list.isEmpty()) {
                    LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.NO_CUSTOMER_ORDER_FOUND)
                            .concat(" http status = ").concat(HttpStatus.NOT_FOUND.name()));

                    Response response = generateResponse(Constants.NO_CUSTOMER_ORDER_FOUND);

                    return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
                } else {
                    LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.SUCCESS)
                            .concat(" http status = ").concat(HttpStatus.OK.name()));

                    return new ResponseEntity<>(list, HttpStatus.OK);
                }
            } else {
                LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.NO_GETTER_FOUND)
                        .concat(" http status = ").concat(HttpStatus.CONFLICT.name()));

                Response response = generateResponse(Constants.NO_GETTER_FOUND);

                return new ResponseEntity<>(response, HttpStatus.CONFLICT);
            }
        }
        LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.ERROR)
                .concat(" http status = ").concat(HttpStatus.BAD_REQUEST.name()));

        Response response = generateResponse(Constants.ERROR);

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
