package com.besprime.web.resource;

import com.besprime.root.entity.Customer;
import com.besprime.root.entity.Stock;
import com.besprime.root.response.Response;
import com.besprime.root.service.interfaces.CustomerService;
import com.besprime.root.service.interfaces.StockService;
import com.besprime.root.util.Constants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class StockResource extends AbstractResource {
    private static transient final Logger LOGGER = Logger.getLogger(StockResource.class);

    @Autowired
    private StockService stockService;

    @Autowired
    private CustomerService customerService;

    public StockService getStockService() {
        return stockService;
    }

    public void setStockService(StockService stockService) {
        this.stockService = stockService;
    }

    public CustomerService getCustomerService() {
        return customerService;
    }

    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @RequestMapping(value = "/stock",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> fetchAllStocks(final @RequestHeader(name = "Authorization", required = false)
                                                    String authorization,
                                            final @RequestHeader(name = "Content-Type", required = false)
                                                    String contentType) {
        if (getValidator().validate(
                new String[]{
                        authorization,
                        contentType}
        ) && contentType.equals(Constants.CONTENT_TYPE_APPLICATION_JSON)) {
            LOGGER.debug(Constants.STATUS_REQ_ENTRY);

            String credentials = getCredentialManager()
                    .decodeBase64(authorization);

            List<String> nameAndEmail = Arrays
                    .asList(credentials.split(":"));

            Customer getter = getCustomerService().
                    findCustomerByNameAndEmail(nameAndEmail.get(0), nameAndEmail.get(1));

            if (getter != null) {
                LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.GETTER_FOUND));

                List<Stock> list = getStockService().findAll();

                if (list == null || list.isEmpty()) {
                    LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.NO_STOCK_FOUND)
                            .concat(" http status = ").concat(HttpStatus.NOT_FOUND.name()));

                    Response response = generateResponse(Constants.NO_STOCK_FOUND);

                    return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
                } else {
                    LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.SUCCESS)
                            .concat(" http status = ").concat(HttpStatus.OK.name()));

                    return new ResponseEntity<>(list, HttpStatus.OK);
                }
            } else {
                LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.NO_GETTER_FOUND)
                        .concat(" http status = ").concat(HttpStatus.CONFLICT.name()));

                Response response = generateResponse(Constants.NO_GETTER_FOUND);

                return new ResponseEntity<>(response, HttpStatus.CONFLICT);
            }
        }
        LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.ERROR)
                .concat(" http status = ").concat(HttpStatus.BAD_REQUEST.name()));

        Response response = generateResponse(Constants.ERROR);

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
