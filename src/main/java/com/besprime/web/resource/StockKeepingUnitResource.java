package com.besprime.web.resource;

import com.besprime.root.entity.Customer;
import com.besprime.root.entity.StockKeepingUnit;
import com.besprime.root.response.Response;
import com.besprime.root.service.interfaces.CustomerService;
import com.besprime.root.service.interfaces.StockKeepingUnitService;
import com.besprime.root.util.Constants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
public class StockKeepingUnitResource extends AbstractResource {
    private static transient final Logger LOGGER = Logger.getLogger(StockKeepingUnitResource.class);

    @Autowired
    private CustomerService customerService;

    @Autowired
    private StockKeepingUnitService stockKeepingUnitService;

    public StockKeepingUnitService getStockKeepingUnitService() {
        return stockKeepingUnitService;
    }

    public void setStockKeepingUnitService(StockKeepingUnitService stockKeepingUnitService) {
        this.stockKeepingUnitService = stockKeepingUnitService;
    }

    public CustomerService getCustomerService() {
        return customerService;
    }

    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @RequestMapping(value = "/sku",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> fetchAllStockKeepingUnits(final @RequestHeader(name = "Authorization", required = false)
                                                               String authorization,
                                                       final @RequestHeader(name = "Content-Type", required = false)
                                                               String contentType) {
        if (getValidator().validate(
                new String[]{
                        authorization,
                        contentType}
        ) && contentType.equals(Constants.CONTENT_TYPE_APPLICATION_JSON)) {
            LOGGER.debug(Constants.STATUS_REQ_ENTRY);

            String credentials = getCredentialManager()
                    .decodeBase64(authorization);

            List<String> nameAndEmail = Arrays
                    .asList(credentials.split(":"));

            Customer getter = getCustomerService().
                    findCustomerByNameAndEmail(nameAndEmail.get(0), nameAndEmail.get(1));

            if (getter != null) {
                LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.GETTER_FOUND));

                List<StockKeepingUnit> stockKeepingUnits = getStockKeepingUnitService().findAll();

                if (stockKeepingUnits == null || stockKeepingUnits.isEmpty()) {
                    LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.NO_SKU_FOUND)
                            .concat(" http status = ").concat(HttpStatus.NOT_FOUND.name()));

                    Response response = generateResponse(Constants.NO_SKU_FOUND);

                    return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
                } else {
                    LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.SUCCESS)
                            .concat(" http status = ").concat(HttpStatus.OK.name()));

                    return new ResponseEntity<>(stockKeepingUnits, HttpStatus.OK);
                }

            } else {
                LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.NO_GETTER_FOUND)
                        .concat(" http status = ").concat(HttpStatus.CONFLICT.name()));

                Response response = generateResponse(Constants.NO_GETTER_FOUND);

                return new ResponseEntity<>(response, HttpStatus.CONFLICT);
            }
        }
        LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.ERROR)
                .concat(" http status = ").concat(HttpStatus.BAD_REQUEST.name()));

        Response response = generateResponse(Constants.ERROR);

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/sku/{skuName}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> fetchStockKeepingUnitByName(final @RequestHeader(name = "Authorization", required = false)
                                                                 String authorization,
                                                         final @RequestHeader(name = "Content-Type", required = false)
                                                                 String contentType,
                                                         final @PathVariable String skuName) {
        if (getValidator().validate(
                new String[]{
                        authorization,
                        contentType,
                        skuName}
        ) && contentType.equals(Constants.CONTENT_TYPE_APPLICATION_JSON)) {
            LOGGER.debug(Constants.STATUS_REQ_ENTRY);

            String credentials = getCredentialManager()
                    .decodeBase64(authorization);

            List<String> nameAndEmail = Arrays
                    .asList(credentials.split(":"));

            Customer getter = getCustomerService().
                    findCustomerByNameAndEmail(nameAndEmail.get(0), nameAndEmail.get(1));

            if (getter != null) {
                LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.GETTER_FOUND));

                StockKeepingUnit stockKeepingUnit = getStockKeepingUnitService().findByName(skuName);

                if (stockKeepingUnit == null) {
                    LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.NO_SKU_FOUND)
                            .concat(" http status = ").concat(HttpStatus.NOT_FOUND.name()));

                    Response response = generateResponse(Constants.NO_SKU_FOUND);

                    return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
                } else {
                    LOGGER.debug(Constants.STATUS_REQ_SUCCESS.concat(" ").concat(Constants.SUCCESS)
                            .concat(" http status = ").concat(HttpStatus.OK.name()));

                    return new ResponseEntity<>(stockKeepingUnit, HttpStatus.OK);
                }

            } else {
                LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.NO_GETTER_FOUND)
                        .concat(" http status = ").concat(HttpStatus.CONFLICT.name()));

                Response response = generateResponse(Constants.NO_GETTER_FOUND);

                return new ResponseEntity<>(response, HttpStatus.CONFLICT);
            }
        }
        LOGGER.warn(Constants.STATUS_REQ_FAIL.concat(" ").concat(Constants.ERROR)
                .concat(" http status = ").concat(HttpStatus.BAD_REQUEST.name()));

        Response response = generateResponse(Constants.ERROR);

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
