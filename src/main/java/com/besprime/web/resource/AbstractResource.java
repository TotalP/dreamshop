package com.besprime.web.resource;

import com.besprime.root.response.Response;
import com.besprime.root.util.CredentialManager;
import com.besprime.root.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.context.ContextLoader;

/**
 * Contains basic validation fields and provides method to build
 * the {@link com.besprime.root.response.Response}
 */
public abstract class AbstractResource {
    @Autowired
    private Validator validator;

    @Autowired
    private CredentialManager credentialManager;

    @Qualifier("webInputParamsValidator")
    public Validator getValidator() {
        return validator;
    }

    public void setValidator(Validator validator) {
        this.validator = validator;
    }

    public CredentialManager getCredentialManager() {
        return credentialManager;
    }

    public void setCredentialManager(CredentialManager credentialManager) {
        this.credentialManager = credentialManager;
    }

    protected Response generateResponse(String message) {
        Response response = ContextLoader.getCurrentWebApplicationContext()
                .getBean(Response.class);
        response.setMessage(message);
        return response;
    }
}
