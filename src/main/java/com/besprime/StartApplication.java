package com.besprime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication(scanBasePackages = {"com.besprime"})
public class StartApplication {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication();
        ApplicationContext context = springApplication.run(StartApplication.class, args);
    }
}
