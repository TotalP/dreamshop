package com.besprime.client;

public enum HttpMethod {
    GET,
    POST,
    DELETE,
    PUT
}
