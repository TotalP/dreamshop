package com.besprime.root.util;

public final class Constants {
    public static final String CONTENT_TYPE_APPLICATION_JSON = "application/json";

    public static final String ERROR = "Error";
    public static final String SUCCESS = "Operation done successful";

    public static final String STATUS_REQ_SUCCESS = "Status: REQ_SUCCESS";
    public static final String STATUS_REQ_ENTRY = "Status: REQ_ENTRY";
    public static final String STATUS_REQ_FAIL = "Status: REQ_FAIL";

    public static final String NO_SKU_FOUND = "No SKU found";
    public static final String NO_SHIPPING_ADDRESS_FOUND = "No shipping address found";
    public static final String NO_CUSTOMER_ORDERS_FOUND = "No customer orders found";
    public static final String NO_STOCK_FOUND = "No stock found";
    public static final String NO_CUSTOMER_FOUND = "No customer found";
    public static final String NO_CUSTOMER_ORDER_FOUND = "No customer order found";
    public static final String NO_GETTER_FOUND = "No getter found";
    public static final String NO_UPDATER_FOUND = "No updater found";
    public static final String NO_CREATOR_FOUND = "No creator found";
    public static final String NO_DELETER_FOUND = "No deleter found";

    public static final String CUSTOMER_FOUND = "Customer found";
    public static final String GETTER_FOUND = "Getter found";
    public static final String UPDATER_FOUND = "Updater found";
    public static final String DELETER_FOUND = "Deleter found";
    public static final String CREATOR_FOUND = "Creator found";

    private Constants() {
        throw new AssertionError();
    }
}
