package com.besprime.root.util;

public interface CredentialManager {

    String encodeBase64(final String input);

    String decodeBase64(final String input);
}