package com.besprime.root.util;

public interface Validator<T> {
    boolean validate(final T... params);
}
