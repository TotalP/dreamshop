package com.besprime.root.util;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;

@Component("credentialManager")
public final class CredentialManagerImpl implements CredentialManager {

    @Override
    public synchronized String encodeBase64(final String input) {
        return "Basic " +
                new String(Base64.encodeBase64(input.getBytes()));
    }

    @Override
    public synchronized String decodeBase64(final String input) {
        return new String(Base64.decodeBase64(input
                .substring(input.indexOf(" ") + 1).getBytes()));
    }
}