package com.besprime.root.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "Stock",
        uniqueConstraints = {
                @UniqueConstraint(name = "stockId",
                        columnNames = "stockId")
        }
)
public final class Stock implements Serializable {
    private long stockId;
    private String name;
    private Set<StockKeepingUnitStock> stockKeepingUnitStocks;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "stockId",
            nullable = false
    )
    @JsonIgnore
    public long getStockId() {
        return stockId;
    }

    public void setStockId(long stockId) {
        this.stockId = stockId;
    }

    @Column(name = "name",
            nullable = false
    )
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "stock")
    @JsonIgnore
    public Set<StockKeepingUnitStock> getStockKeepingUnitStocks() {
        return stockKeepingUnitStocks;
    }

    public void setStockKeepingUnitStocks(Set<StockKeepingUnitStock> stockKeepingUnitStocks) {
        this.stockKeepingUnitStocks = stockKeepingUnitStocks;
    }
}
