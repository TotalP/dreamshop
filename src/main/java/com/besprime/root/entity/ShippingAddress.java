package com.besprime.root.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ShippingAddress",
        uniqueConstraints = {
                @UniqueConstraint(name = "shippingAddressId",
                        columnNames = "shippingAddressId")
        }
)
public final class ShippingAddress implements Serializable {
    private long shippingAddressId;
    private String postalCode;
    private String street;
    private short house;
    private short flat;
    private Customer customer;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "shippingAddressId",
            nullable = false
    )
    public long getShippingAddressId() {
        return shippingAddressId;
    }

    public void setShippingAddressId(long shippingAddressId) {
        this.shippingAddressId = shippingAddressId;
    }

    @Column(name = "postalCode",
            nullable = false
    )
    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Column(name = "street",
            nullable = false
    )
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Column(name = "house",
            nullable = false
    )
    public short getHouse() {
        return house;
    }

    public void setHouse(short house) {
        this.house = house;
    }

    @Column(name = "flat",
            nullable = false
    )
    public short getFlat() {
        return flat;
    }

    public void setFlat(short flat) {
        this.flat = flat;
    }

    @ManyToOne
    @JoinColumn(name = "customerId",
            nullable = false,
            foreignKey = @ForeignKey(name = "FK_shippingAddress_customer")
    )
    @JsonIgnore
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShippingAddress that = (ShippingAddress) o;

        if (shippingAddressId != that.shippingAddressId) return false;
        if (house != that.house) return false;
        if (flat != that.flat) return false;
        if (postalCode != null ? !postalCode.equals(that.postalCode) : that.postalCode != null) return false;
        return street != null ? street.equals(that.street) : that.street == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (shippingAddressId ^ (shippingAddressId >>> 32));
        result = 31 * result + (postalCode != null ? postalCode.hashCode() : 0);
        result = 31 * result + (street != null ? street.hashCode() : 0);
        result = 31 * result + (int) house;
        result = 31 * result + (int) flat;
        return result;
    }
}
