package com.besprime.root.entity.specification;

import com.besprime.root.entity.StockKeepingUnit;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import java.util.ArrayList;
import java.util.List;

public class StockKeepingUnitSpecificationsBuilder {
    private final List<SearchCriteria> params;

    public StockKeepingUnitSpecificationsBuilder() {
        params = new ArrayList<SearchCriteria>();
    }

    public StockKeepingUnitSpecificationsBuilder with(String key, String operation, Object value) {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setKey(key);
        searchCriteria.setOperation(operation);
        searchCriteria.setValue(value);
        params.add(searchCriteria);
        return this;
    }

    public Specification<StockKeepingUnit> build() {
        if (params.size() == 0) {
            return null;
        }

        List<Specification<StockKeepingUnit>> specs = new ArrayList<Specification<StockKeepingUnit>>();
        for (SearchCriteria param : params) {
            StockKeepingUnitSpecification specification = new StockKeepingUnitSpecification();
            specification.setCriteria(param);
        }

        Specification<StockKeepingUnit> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).and(specs.get(i));
        }
        return result;
    }
}
