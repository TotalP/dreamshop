package com.besprime.root.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "StockKeepingUnit",
        uniqueConstraints = {
                @UniqueConstraint(name = "stockKeepingUnitId",
                        columnNames = "stockKeepingUnitId")
        }
)
public final class StockKeepingUnit implements Serializable {
    private long stockKeepingUnitId;
    private String name;
    private long price;
    private CustomerOrder customerOrder;
    private Set<StockKeepingUnitStock> stockKeepingUnitStocks;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "stockKeepingUnitId",
            nullable = false
    )
    public long getStockKeepingUnitId() {
        return stockKeepingUnitId;
    }

    public void setStockKeepingUnitId(long stockKeepingUnitId) {
        this.stockKeepingUnitId = stockKeepingUnitId;
    }

    @Column(name = "name",
            nullable = false
    )
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "price",
            nullable = false
    )
    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "stockKeepingUnit")
    @JsonProperty(value = "Amounts")
    public Set<StockKeepingUnitStock> getStockKeepingUnitStocks() {
        return stockKeepingUnitStocks;
    }

    public void setStockKeepingUnitStocks(Set<StockKeepingUnitStock> stockKeepingUnitStocks) {
        this.stockKeepingUnitStocks = stockKeepingUnitStocks;
    }

    @ManyToOne
    @JoinColumn(name = "customerOrderId",
            nullable = true,
            foreignKey = @ForeignKey(name = "FK_stockKeepingUnit_customerOrder")
    )
    @JsonIgnore
    public CustomerOrder getCustomerOrder() {
        return customerOrder;
    }

    public void setCustomerOrder(CustomerOrder customerOrder) {
        this.customerOrder = customerOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StockKeepingUnit that = (StockKeepingUnit) o;

        if (stockKeepingUnitId != that.stockKeepingUnitId) return false;
        if (price != that.price) return false;
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (stockKeepingUnitId ^ (stockKeepingUnitId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) (price ^ (price >>> 32));
        return result;
    }
}
