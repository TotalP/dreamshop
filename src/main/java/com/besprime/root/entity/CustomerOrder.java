package com.besprime.root.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CustomerOrder",
        uniqueConstraints = {
                @UniqueConstraint(name = "customerOrderId",
                        columnNames = "customerOrderId")
        }
)
public final class CustomerOrder implements Serializable {
    private long customerOrderId;
    private List<StockKeepingUnit> stockKeepingUnits;
    private Customer customer;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customerOrderId",
            nullable = false
    )
    public long getCustomerOrderId() {
        return customerOrderId;
    }

    public void setCustomerOrderId(long customerOrderId) {
        this.customerOrderId = customerOrderId;
    }

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "customerOrder"
    )
    public List<StockKeepingUnit> getStockKeepingUnits() {
        if (stockKeepingUnits == null) {
            stockKeepingUnits = new ArrayList<>();
        }
        return stockKeepingUnits;
    }

    public void setStockKeepingUnits(List<StockKeepingUnit> stockKeepingUnits) {
        this.stockKeepingUnits = stockKeepingUnits;
    }

    @ManyToOne
    @JoinColumn(name = "customerId",
            nullable = false,
            foreignKey = @ForeignKey(name = "FK_customerOrder_customer")
    )
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
