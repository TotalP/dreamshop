package com.besprime.root.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Customer",
        uniqueConstraints = {
                @UniqueConstraint(name = "customerId",
                        columnNames = "customerId")
        }
)
public final class Customer implements Serializable {
    private long customerId;
    private String name;
    private String email;
    private long credit;
    private Set<ShippingAddress> shippingAddresses;
    private Set<CustomerOrder> customerOrders;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customerId",
            nullable = false
    )
    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    @Column(name = "name",
            nullable = false
    )
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "email",
            nullable = false
    )
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "credit",
            nullable = false
    )
    public long getCredit() {
        return credit;
    }

    public void setCredit(long credit) {
        this.credit = credit;
    }

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "customer"
    )
    @JsonIgnore
    public Set<ShippingAddress> getShippingAddresses() {
        if (shippingAddresses == null) {
            shippingAddresses = new HashSet<>();
        }
        return shippingAddresses;
    }

    public void setShippingAddresses(Set<ShippingAddress> shippingAddresses) {
        this.shippingAddresses = shippingAddresses;
    }

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "customer"
    )
    @JsonIgnore
    public Set<CustomerOrder> getCustomerOrders() {
        if (customerOrders == null) {
            customerOrders = new HashSet<>();
        }
        return customerOrders;
    }

    public void setCustomerOrders(Set<CustomerOrder> customerOrders) {
        this.customerOrders = customerOrders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        if (customerId != customer.customerId) return false;
        if (credit != customer.credit) return false;
        if (name != null ? !name.equals(customer.name) : customer.name != null) return false;
        return email != null ? email.equals(customer.email) : customer.email == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (customerId ^ (customerId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (int) (credit ^ (credit >>> 32));
        return result;
    }
}
