package com.besprime.root.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "StockKeepingUnitStock")
public class StockKeepingUnitStock implements Serializable {
    private StockKeepingUnit stockKeepingUnit;
    private Stock stock;
    private long amount;

    @Id
    @ManyToOne
    @JoinColumn(name = "stockKeepingUnitId",
            nullable = false,
            foreignKey = @ForeignKey(name = "FK_stockKeepingUnitStock_stockKeepingUnit")
    )
    @JsonIgnore
    public StockKeepingUnit getStockKeepingUnit() {
        return stockKeepingUnit;
    }

    public void setStockKeepingUnit(StockKeepingUnit stockKeepingUnit) {
        this.stockKeepingUnit = stockKeepingUnit;
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "stockId",
            nullable = false,
            foreignKey = @ForeignKey(name = "FK_stockKeepingUnitStock_stock")
    )
    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    @Column(name = "amount")
    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }
}
