package com.besprime.root.response;

import java.util.HashMap;
import java.util.Map;

public class CheckoutRequest {
    private Map<String, Long> stockKeepingUnits;

    public CheckoutRequest() {
        this.stockKeepingUnits = new HashMap<>();
    }

    public Map<String, Long> getStockKeepingUnits() {
        return stockKeepingUnits;
    }

    public void setStockKeepingUnits(Map<String, Long> stockKeepingUnits) {
        this.stockKeepingUnits = stockKeepingUnits;
    }
}
