package com.besprime.root.response;

import com.besprime.root.entity.StockKeepingUnit;

import java.util.List;

public class CheckoutResponse {
    private List<StockKeepingUnit> validSKU;
    private List<String> invalidSKU;
    private List<String> lessSKU;
    private Long totalAmount;

    public List<StockKeepingUnit> getValidSKU() {
        return validSKU;
    }

    public void setValidSKU(List<StockKeepingUnit> validSKU) {
        this.validSKU = validSKU;
    }

    public List<String> getInvalidSKU() {
        return invalidSKU;
    }

    public void setInvalidSKU(List<String> invalidSKU) {
        this.invalidSKU = invalidSKU;
    }

    public List<String> getLessSKU() {
        return lessSKU;
    }

    public void setLessSKU(List<String> lessSKU) {
        this.lessSKU = lessSKU;
    }

    public Long getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Long totalAmount) {
        this.totalAmount = totalAmount;
    }
}
