package com.besprime.root.service.impl;

import com.besprime.root.entity.StockKeepingUnit;
import com.besprime.root.repository.StockKeepingUnitRepository;
import com.besprime.root.service.interfaces.StockKeepingUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
@Service("stockKeepingUnitService")
public final class StockKeepingUnitServiceImpl implements StockKeepingUnitService {

    @Autowired
    private StockKeepingUnitRepository stockKeepingUnitRepository;

    public StockKeepingUnitRepository getStockKeepingUnitRepository() {
        return stockKeepingUnitRepository;
    }

    public void setStockKeepingUnitRepository(StockKeepingUnitRepository stockKeepingUnitRepository) {
        this.stockKeepingUnitRepository = stockKeepingUnitRepository;
    }

    @Override
    public List<StockKeepingUnit> findAll() {
        List<StockKeepingUnit> list = new ArrayList<>();
        for (StockKeepingUnit item : getStockKeepingUnitRepository().findAll()) {
            list.add(item);
        }
        return list;
    }

    @Override
    public List<StockKeepingUnit> findAll(final Integer pageIndex, final Integer numRecPerPage) {
        Sort sort = new Sort(Sort.Direction.ASC, "stockKeepingUnitName");
        /*
        * @param page zero-based page index.
        * @param size the size of the page to be returned.
        * @param sort can be {@literal null}.
        */
        return getStockKeepingUnitRepository()
                .findAll(new PageRequest(pageIndex, numRecPerPage, sort)).getContent();
    }

    @Override
    public StockKeepingUnit findByName(String stockKeepingUnitName) {
        List<StockKeepingUnit> stockKeepingUnits = getStockKeepingUnitRepository().findByName(stockKeepingUnitName);
        return (stockKeepingUnits != null && !stockKeepingUnits.isEmpty()) ? stockKeepingUnits.get(0) : null;
    }

    @Override
    public StockKeepingUnit save(StockKeepingUnit entity) {
        return getStockKeepingUnitRepository().save(entity);
    }

    @Override
    public StockKeepingUnit update(StockKeepingUnit entity) {
        return getStockKeepingUnitRepository().save(entity);
    }

    @Override
    public void deleteStockKeepingUnitByStockKeepingUnitId(Long stockKeepingUnitId) {
        getStockKeepingUnitRepository().delete(stockKeepingUnitId);
    }
}
