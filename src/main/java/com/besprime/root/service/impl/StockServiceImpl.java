package com.besprime.root.service.impl;

import com.besprime.root.entity.Stock;
import com.besprime.root.repository.StockRepository;
import com.besprime.root.service.interfaces.StockService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
@Service("stockService")
public class StockServiceImpl implements StockService {

    @Autowired
    private StockRepository stockRepository;

    public StockRepository getStockRepository() {
        return stockRepository;
    }

    public void setStockRepository(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

    @Override
    public List<Stock> findAll() {
        return getStockRepository().findAll();
    }

    @Override
    public Stock findByStockId(Long stockId) {
        return getStockRepository().findOne(stockId);
    }

    @Override
    public Stock findByStockName(String stockName) {
        List<Stock> list = getStockRepository().findByName(stockName);
        return (list != null && !list.isEmpty()) ? list.get(0) : null;
    }

    @Override
    public Stock save(Stock entity) {
        return getStockRepository().save(entity);
    }

    @Override
    public Stock update(Stock entity) {
        return getStockRepository().save(entity);
    }

    @Override
    public void deleteStockByStockId(Long stockId) {
        getStockRepository().delete(stockId);
    }
}
