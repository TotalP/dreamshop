package com.besprime.root.service.impl;

import com.besprime.root.entity.CustomerOrder;
import com.besprime.root.repository.CustomerOrderRepository;
import com.besprime.root.service.interfaces.CustomerOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
@Service("customerOrderService")
public class CustomerOrderServiceImpl implements CustomerOrderService {

    @Autowired
    private CustomerOrderRepository customerOrderRepository;

    public CustomerOrderRepository getCustomerOrderRepository() {
        return customerOrderRepository;
    }

    public void setCustomerOrderRepository(CustomerOrderRepository customerOrderRepository) {
        this.customerOrderRepository = customerOrderRepository;
    }

    @Override
    public List<CustomerOrder> findAll() {
        return getCustomerOrderRepository().findAll();
    }

    @Override
    public CustomerOrder save(CustomerOrder entity) {
        return getCustomerOrderRepository().save(entity);
    }
}
