package com.besprime.root.service.impl;

import com.besprime.root.entity.Customer;
import com.besprime.root.repository.CustomerRepository;
import com.besprime.root.service.interfaces.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
@Service("customerService")
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public CustomerRepository getCustomerRepository() {
        return customerRepository;
    }

    public void setCustomerRepository(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> findAll() {
        return getCustomerRepository().findAll();
    }

    @Override
    public Customer findCustomerByName(String name) {
        List<Customer> customers = getCustomerRepository().findByName(name);
        return (customers != null && !customers.isEmpty()) ? customers.get(0) : null;
    }

    @Override
    public Customer findCustomerByNameAndEmail(String name, String email) {
        List<Customer> customers = getCustomerRepository().findByNameAndEmail(name, email);
        return (customers != null && !customers.isEmpty()) ? customers.get(0) : null;
    }

    @Override
    public Customer save(Customer entity) {
        return getCustomerRepository().save(entity);
    }
}
