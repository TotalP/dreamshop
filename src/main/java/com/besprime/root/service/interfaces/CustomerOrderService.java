package com.besprime.root.service.interfaces;

import com.besprime.root.entity.CustomerOrder;

import java.util.List;

public interface CustomerOrderService {
    List<CustomerOrder> findAll();

    CustomerOrder save(final CustomerOrder entity);
}
