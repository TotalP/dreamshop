package com.besprime.root.service.interfaces;

import com.besprime.root.entity.Customer;

import java.util.List;

public interface CustomerService {
    List<Customer> findAll();

    Customer findCustomerByName(final String name);

    Customer findCustomerByNameAndEmail(String name, String email);

    Customer save(final Customer entity);
}
