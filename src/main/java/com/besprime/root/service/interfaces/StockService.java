package com.besprime.root.service.interfaces;

import com.besprime.root.entity.Stock;

import java.util.List;

public interface StockService {
    List<Stock> findAll();

    Stock findByStockId(final Long stockId);

    Stock findByStockName(final String stockName);

    Stock save(final Stock entity);

    Stock update(final Stock entity);

    void deleteStockByStockId(final Long stockId);
}
