package com.besprime.root.service.interfaces;

import com.besprime.root.entity.StockKeepingUnit;

import java.util.List;

public interface StockKeepingUnitService {
    List<StockKeepingUnit> findAll();

    List<StockKeepingUnit> findAll(final Integer pageIndex, final Integer numRecPerPage);

    StockKeepingUnit findByName(final String stockKeepingUnitName);

    StockKeepingUnit save(final StockKeepingUnit entity);

    StockKeepingUnit update(final StockKeepingUnit entity);

    void deleteStockKeepingUnitByStockKeepingUnitId(final Long stockKeepingUnitId);
}
