package com.besprime.root.repository;

import com.besprime.root.entity.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StockRepository extends JpaRepository<Stock, Long> {
    List<Stock> findByName(final String name);

    List<Stock> findByStockId(final long stockId);
}
