package com.besprime.root.repository;

import com.besprime.root.entity.StockKeepingUnit;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface StockKeepingUnitRepository extends PagingAndSortingRepository<StockKeepingUnit, Long>,
        JpaSpecificationExecutor<StockKeepingUnit> {
    List<StockKeepingUnit> findByName(final String name);

    List<StockKeepingUnit> findByStockKeepingUnitIdAndName(final String stockKeepingUnitId,
                                                           final String name);
}