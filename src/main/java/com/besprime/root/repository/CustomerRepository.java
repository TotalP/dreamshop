package com.besprime.root.repository;

import com.besprime.root.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    List<Customer> findByName(final String name);

    List<Customer> findByNameAndEmail(String name, String email);
}