INSERT INTO DreamShop.ShippingAddress (flat, house, postalCode, street, customerId) VALUES
  (12, 150, "POSTAL_CODE_1", "Street_1", 1),
  (29, 56, "POSTAL_CODE_2", "Street_2", 1),
  (8, 3, "POSTAL_CODE_3", "Street_3", 1),
  (5, 30, "POSTAL_CODE_4", "Street_3", 2),
  (9, 1, "POSTAL_CODE_2", "Street_2", 3),
  (20, 16, "POSTAL_CODE_5", "Street_5", 3);