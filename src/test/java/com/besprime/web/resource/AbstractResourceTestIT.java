package com.besprime.web.resource;

public abstract class AbstractResourceTestIT {
    private static final String SLASH = "/";

    private static final String HOST = "localhost";

    private static final String COLON = ":";

    private static final Integer PORT = 8090;

    private static final String ROOT_URL = "DreamShop";

    protected String getBaseUrl() {
        return new StringBuilder()
                .append(COLON)
                .append(SLASH)
                .append(SLASH)
                .append(HOST)
                .append(COLON)
                .append(PORT)
                .append(SLASH)
                .append(ROOT_URL)
                .append(SLASH)
                .toString();
    }
}
