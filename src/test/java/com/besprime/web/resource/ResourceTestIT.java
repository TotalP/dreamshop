package com.besprime.web.resource;

import com.besprime.client.HttpExecutor;
import com.besprime.client.Protocol;
import com.besprime.root.entity.Customer;
import com.besprime.root.entity.ShippingAddress;
import com.besprime.root.entity.Stock;
import com.besprime.root.entity.StockKeepingUnit;
import com.besprime.root.util.Constants;
import com.besprime.root.util.CredentialManager;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResourceTestIT extends AbstractResourceTestIT {
    private static final Logger LOGGER = Logger.getLogger(ResourceTestIT.class);

    private static final ApplicationContext APPLICATION_CONTEXT =
            new ClassPathXmlApplicationContext(
                    new String[]{"applicationContext.xml"});

    private HttpExecutor httpExecutor;

    private CredentialManager credentialManager;

    private final Map<String, String> headers = new HashMap<>();

    @Before
    public void setUp() throws Exception {
        httpExecutor = (HttpExecutor) APPLICATION_CONTEXT
                .getBean("httpExecutor");

        credentialManager = (CredentialManager) APPLICATION_CONTEXT
                .getBean("credentialManager");

        Assert.assertNotNull(credentialManager);
        Assert.assertNotNull(httpExecutor);
    }

    @Test
    public void testFetchAllCustomersIT() throws Exception {
        headers.put("Content-Type", Constants.CONTENT_TYPE_APPLICATION_JSON);
        headers.put("Authorization", credentialManager.encodeBase64("customerName1:customerName1@gmail.com"));

        final String response = httpExecutor
                .executeGet(Protocol.HTTP.name().concat(getBaseUrl()).concat("customer"),
                        headers,
                        "");

        List<Customer> customers = new ObjectMapper()
                .readValue(response, new TypeReference<List<Customer>>() {
                });

        Assert.assertEquals(3, customers.size());
        for (int i = 1; i <= customers.size(); i++) {
            Assert.assertEquals("customerName".concat(String.valueOf(i)).concat("@gmail.com"),
                    customers.get(i - 1).getEmail());
        }
    }

    @Test
    public void testFetchStockKeepingUnitsIT() throws Exception {
        headers.put("Content-Type", Constants.CONTENT_TYPE_APPLICATION_JSON);
        headers.put("Authorization", credentialManager.encodeBase64("customerName1:customerName1@gmail.com"));

        final String response = httpExecutor
                .executeGet(Protocol.HTTP.name().concat(getBaseUrl()).concat("sku"),
                        headers, "");

        List<StockKeepingUnit> stockKeepingUnits = new ObjectMapper()
                .readValue(response, new TypeReference<List<StockKeepingUnit>>() {
                });

        LOGGER.debug(response);

        Assert.assertEquals(10, stockKeepingUnits.size());
    }

    @Test
    public void testFetchAllShippingAddressesIT() throws Exception {
        headers.put("Content-Type", Constants.CONTENT_TYPE_APPLICATION_JSON);
        headers.put("Authorization", credentialManager.encodeBase64("customerName1:customerName1@gmail.com"));

        final String response = httpExecutor
                .executeGet(Protocol.HTTP.name().concat(getBaseUrl()).concat("customer").concat("/").concat("customerName1").concat("/").concat("shippingAddress"),
                        headers, "");

        List<ShippingAddress> shippingAddresses = new ObjectMapper()
                .readValue(response, new TypeReference<List<ShippingAddress>>() {
                });

        LOGGER.debug(response);

        Assert.assertEquals(3, shippingAddresses.size());
    }

    @Test
    public void testFetchAllStocksIT() throws Exception {
        headers.put("Content-Type", Constants.CONTENT_TYPE_APPLICATION_JSON);
        headers.put("Authorization", credentialManager.encodeBase64("customerName1:customerName1@gmail.com"));

        final String response = httpExecutor
                .executeGet(Protocol.HTTP.name().concat(getBaseUrl()).concat("stock"),
                        headers, "");

        List<Stock> stocks = new ObjectMapper()
                .readValue(response, new TypeReference<List<Stock>>() {
                });

        LOGGER.debug(response);

        Assert.assertEquals(3, stocks.size());
    }
}
