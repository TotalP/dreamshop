package com.besprime.root.entity;

import com.besprime.root.response.CheckoutRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

public class CustomerOrderTest {

    public static final Logger LOGGER = Logger.getLogger(CustomerOrderTest.class);

    @Test
    public void testCustomerOrder() throws Exception {
        final String customerOrder = "{\"stockKeepingUnits\":{\"StockKeepingUnit_1\":4,\"StockKeepingUnit_2\":2}}";

        CheckoutRequest request = new ObjectMapper()
                .readValue(customerOrder, new TypeReference<CheckoutRequest>() {});

        Assert.assertNotNull(request);
        Assert.assertEquals(2, request.getStockKeepingUnits().size());
        Assert.assertEquals(Long.valueOf(4), request.getStockKeepingUnits().get("StockKeepingUnit_1"));
        Assert.assertEquals(Long.valueOf(2), request.getStockKeepingUnits().get("StockKeepingUnit_2"));
    }
}
